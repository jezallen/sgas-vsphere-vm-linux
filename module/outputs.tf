output "default_ip_address" {
 value = vsphere_virtual_machine.vsphere_vm_linux.*.default_ip_address
}
output "name" {
 value = vsphere_virtual_machine.vsphere_vm_linux.*.name
}