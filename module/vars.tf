variable "vm_count" {}
variable "global_domain" {}
variable "global_ipv4_gateway" {}
variable "vcenter_dc_name" {}
variable "vcenter_compute_cluster" {}
variable "vcenter_datastore_cluster" {}
variable "vcenter_network_name" {}

variable "global_dns_servers" {
    type = list(string)
}

variable "vm_template_name" {}
variable "vm_name_prefix" {}
//variable "vm_guest_id" {}
variable "vm_disk_size_gb" {}
variable "vm_disk_name" {}
variable "vm_subnet_first_address" {}
variable "vm_subnetmask" {}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------
  variable "vm_num_cpus" {
    description = "Number of cpus in this VM"
    default = 2
    type = number
  }

  variable "vm_memory" {
    description = "Memory (RAM) size for this VM"
    default = 2048
    type = number
  }