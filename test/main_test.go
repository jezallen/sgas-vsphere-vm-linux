package test

import (
	"fmt"
	"net"
	"runtime"
	"strings"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/terraform"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"
	"github.com/stretchr/testify/assert"
	"github.com/tatsushid/go-fastping"
)

// Main function, define stages and run.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Entry point has to be declared like this - func TestXxx(*testing.T) - must have upper case T
func Test_Main(t *testing.T) {
	t.Parallel()

	// set directory containing Terraform code
	workingDir := "..//examples/linux"
	// we're using Terragrunt - default is terraform
	terraformBinary := "terragrunt"
	//terraformBinary := "terraform"

	// At the end of the test, destroy deployed resources
	defer test_structure.RunTestStage(t, "cleanup", func() {
		destroyResources(t, workingDir)
	})

	// Deploy resources with Terraform
	test_structure.RunTestStage(t, "deploy", func() {
		deployResources(t, workingDir, terraformBinary)
	})

	// Validate
	test_structure.RunTestStage(t, "validate", func() {
		secondsBetweenRetries := 10
		maximumRetries := 10
		assert.True(t, validateDeployedVMsByPing(t, workingDir, secondsBetweenRetries, maximumRetries))
	})
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Deploy the resources using Terraform
func deployResources(t *testing.T, workingDir string, terraformBinary string) {
	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformBinary: terraformBinary,
		TerraformDir:    workingDir,
	}

	// Save the Terraform Options struct, instance name, and instance text so future test stages can use it
	test_structure.SaveTerraformOptions(t, workingDir, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Destroy the resources
func destroyResources(t *testing.T, workingDir string) {
	// Load the Terraform Options saved by the earlier deploy_terraform stage
	terraformOptions := test_structure.LoadTerraformOptions(t, workingDir)
	terraform.Destroy(t, terraformOptions)
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func validateDeployedVMsByPing(t *testing.T, workingDir string, secondsBetweenRetries int, maxRetries int) bool {
	// Load the Terraform Options saved by the earlier deploy_terraform stage
	terraformOptions := test_structure.LoadTerraformOptions(t, workingDir)

	// detect OS & if linux, set "ping" protocol to udp
	// this is necessary because non-root on linux will
	// always fail with "socket: permission denied" error
	// if protocol is ip
	protocol := "ip"
	if runtime.GOOS == "linux" {
		protocol = "udp"
	}
	fmt.Println("*** - Protocol is", protocol)

	// Put the ip address output from Terraform into an array
	// (Output is a string, needs some work to make it an array)
	terraformOutput := terraform.Output(t, terraformOptions, "vm_addresses")
	replacer := strings.NewReplacer("[", "", "]", "", "\n", "", " ", "", "\"", "")
	parsedList := replacer.Replace(terraformOutput)
	addressArray := strings.Split(parsedList, ",")

	// flag indicating that all IP addresses pinged sucessfully
	// (and therefore we don't need to retry)
	var allPingedOK bool

	for j := 1; j <= maxRetries; j++ {
		fmt.Println("*** Retry: ", j)
		allPingedOK = true
		for i := range addressArray {
			if len(addressArray[i]) != 0 {
				fmt.Println("*** Pinging: " + addressArray[i])
				vmPingTest := PingSingleIPv4Address(addressArray[i], protocol)
				fmt.Println("***", vmPingTest)

				// setting pingOK to false
				// will trigger a retry (even if only one has failed)
				if vmPingTest == false {
					allPingedOK = false
				}
			}
		}
		// all replied to ping request
		if allPingedOK == true {
			return true
		}
		// at least one failed
		// retry after pause
		time.Sleep(time.Duration(secondsBetweenRetries) * time.Second)
		continue
	}
	// still failing after maxRetries
	return false
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func PingSingleIPv4Address(ipAddress string, protocol string) bool {

	var pingResult bool

	p := fastping.NewPinger()
	ra, err := net.ResolveIPAddr("ip4:icmp", ipAddress)

	p.MaxRTT = time.Millisecond * 1000
	p.Network(protocol)
	p.AddIPAddr(ra)
	p.OnRecv = func(addr *net.IPAddr, rtt time.Duration) {
		pingResult = true
	}

	err = p.Run()
	if err != nil {
		fmt.Println("*** ", err)
		pingResult = false
	}
	return pingResult
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
