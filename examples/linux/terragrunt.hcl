inputs = {
  vm_template_name = "JPA_CentOS7_001"
  vm_name_prefix = "Nobby"
  vm_num_cpus = 2
  vm_memory = 1024
  vm_disk_size_gb = 20
  vm_disk_name = "disk0"
  vm_subnet_first_address = "192.168.1.30"
  vm_subnetmask = 24
}

include {
  path = find_in_parent_folders()
}