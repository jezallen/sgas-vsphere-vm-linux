provider "vsphere" {
  vsphere_server     = var.vcenter_server
  user               = var.vcenter_user
  password           = var.vcenter_pw
  
  # If you have a self-signed cert
  allow_unverified_ssl = true
}

module "vsphere_vm_linux" {
  source = "../..//module"

  //set number of instances created here
  vm_count = 1

  //these from the root terragrunt.hcl
  global_domain = var.global_domain
  global_ipv4_gateway = var.global_ipv4_gateway
  global_dns_servers = var.global_dns_servers
  vcenter_dc_name = var.vcenter_dc_name
  vcenter_compute_cluster = var.vcenter_compute_cluster
  vcenter_datastore_cluster = var.vcenter_datastore_cluster
  vcenter_network_name = var.vcenter_network_name

  //and these from this level terragrunt.hcl
  vm_template_name = var.vm_template_name
  vm_name_prefix = var.vm_name_prefix
  vm_num_cpus = var.vm_num_cpus
  vm_memory = var.vm_memory
  vm_disk_size_gb = var.vm_disk_size_gb
  vm_disk_name = var.vm_disk_name
  vm_subnet_first_address = var.vm_subnet_first_address
  vm_subnetmask = var.vm_subnetmask
}