//outputs from module vsphere_linux_vm
output "vm_names" {
value = module.vsphere_vm_linux.name
}
output "vm_addresses" {
value = module.vsphere_vm_linux.default_ip_address
}