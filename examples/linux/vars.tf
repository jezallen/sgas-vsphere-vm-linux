variable "vm_count" {}
variable "vcenter_server" {}
variable "vcenter_user" {}
variable "vcenter_pw" {}
variable "vcenter_dc_name" {}
variable "vcenter_compute_cluster" {}
variable "vcenter_datastore_cluster" {}
variable "vcenter_network_name" {}

variable "global_domain" {}
variable "global_ipv4_gateway" {}
variable "global_dns_servers" {
    type = list(string)
}

variable "vm_template_name" {}
variable "vm_name_prefix" {}
variable "vm_num_cpus" {}
variable "vm_memory" {}
variable "vm_disk_size_gb" {}
variable "vm_disk_name" {}
variable "vm_subnet_first_address" {}
variable "vm_subnetmask" {}

