inputs = {
  //VCenter stuff
  vcenter_server = "192.168.1.11"
  vcenter_user = "administrator@groveland.com"
  vcenter_pw = "password-goes-here"
  vcenter_dc_name = "groveland"
  vcenter_compute_cluster = "Groveland-Cluster-001"
  vcenter_datastore_cluster = "Groveland-DS-Cluster-001"
  vcenter_network_name = "VM Network"

  // other environment constants
  vm_count = 1
  global_domain = "groveland.com"
  global_ipv4_gateway = "192.168.1.1"
  global_dns_servers = ["8.8.8.8", "4.4.4.4"]
}